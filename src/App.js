import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import "./components/Products.css";
import AllProducts from "./components/AllProducts";
import HeaderContent from "./components/Header";
import CreateProduct from "./components/CreateProduct";
import axios from "axios";
import DeleteProduct from "./components/DeleteProduct";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDataFetched: false,
      isCreateFetchFailed: false,
      createFetchError: "",
      updateFetchError: false,
      deleteFetchError: false,
      DeleteSuccessMessage: "",
      deleteFailMessage: "",
      updateFetcherrMessage: "",
      createFetchSuccess: "",
      FetchedData: "",
      isFetchingFailed: false,
      isFetchDataFetched: false,
      selectedProduct: [],
      isElementClicked: false,
      isSaveClicked: false,
      titleField: "",
      descriptionField: "",
      priceField: "",
      imageField: "",
      categoryField: "",
      titleTobeAdded: "",
      priceToBeAdded: "",
      descriptionTobeAdded: "",
      ImageTobeAdded: "",
      categoryTobeAdded: "",
      prodTobeDeleted: "",
    };
  }

  newTitle = (value) => {
    console.log("title", value);
    this.setState({
      titleTobeAdded: value,
    });
  };

  newDescription = (value) => {
    console.log("desc", value);
    this.setState({
      descriptionTobeAdded: value,
    });
  };

  newCategory = (value) => {
    console.log("cat", value);
    this.setState({
      categoryTobeAdded: value,
    });
  };

  newImage = (value) => {
    console.log("img", value);
    this.setState({
      ImageTobeAdded: value,
    });
  };

  newPrice = (value) => {
    console.log("price", value);
    this.setState({
      priceToBeAdded: value,
    });
  };

  componentDidMount() {
    axios
      .get("https://fakestoreapi.com/products")
      .then((res) => {
        console.log(res.data);
        this.setState({
          isDataFetched: true,
          FetchedData: res.data,
          isFetchingFailed: false,
        });
      })
      .catch((err) => {
        this.setState({
          isFetchingFailed: true,
        });
      });
  }

  getClickedProductDetails = (clickedId) => {
    let receivedData = this.state.FetchedData;
    let matchedId = receivedData.filter(
      (eachProductData) => eachProductData.id === Number(clickedId)
    );
    this.setState({
      selectedProduct: matchedId[0],
      isElementClicked: true,
      titleField: matchedId[0].title,
      descriptionField: matchedId[0].description,
      priceField: matchedId[0].price,
      imageField: matchedId[0].image,
      categoryField: matchedId[0].category,
    });
  };

  updateTitle = (event) => {
    const clickedId = this.state.selectedProduct.id;

    const data = this.state.FetchedData.map((eachProduct) => {
      if (eachProduct.id === clickedId) {
        eachProduct["title"] = event.target.value;
        return eachProduct;
      }
      return eachProduct;
    });

    this.setState({
      FetchedData: data,
      titleField: event.target.value,
    });
  };

  updateDescription = (event) => {
    const clickedId = this.state.selectedProduct.id;

    const data = this.state.FetchedData.map((eachProduct) => {
      if (eachProduct.id === clickedId) {
        eachProduct["description"] = event.target.value;
        return eachProduct;
      }
      return eachProduct;
    });

    this.setState({
      FetchedData: data,
      descriptionField: event.target.value,
    });
  };

  updatePrice = (event) => {
    const clickedId = this.state.selectedProduct.id;

    const data = this.state.FetchedData.map((eachProduct) => {
      if (eachProduct.id === clickedId) {
        eachProduct["price"] = event.target.value;
        return eachProduct;
      }
      return eachProduct;
    });

    this.setState({
      FetchedData: data,
      priceField: event.target.value,
    });
  };

  updateCategory = (event) => {
    const clickedId = this.state.selectedProduct.id;

    const data = this.state.FetchedData.map((eachProduct) => {
      if (eachProduct.id === clickedId) {
        eachProduct["category"] = event.target.value;
        return eachProduct;
      }
      return eachProduct;
    });

    this.setState({
      FetchedData: data,
      categoryField: event.target.value,
    });
  };
  updateImage = (event) => {
    const clickedId = this.state.selectedProduct.id;

    const data = this.state.FetchedData.map((eachProduct) => {
      if (eachProduct.id === clickedId) {
        eachProduct["image"] = event.target.value;
        return eachProduct;
      }
      return eachProduct;
    });

    this.setState({
      FetchedData: data,
      imageField: event.target.value,
    });
  };

  onSaveFn = (id) => {
    console.log(id);
    let updatedDataObj = {
      title: this.state.titleField,
      price: this.state.priceField,
      description: this.state.descriptionField,
      image: this.state.imageField,
      category: this.state.categoryField,
    };

    axios
      .put(`https://fakestoreapi.com/products/${id}`, updatedDataObj)
      .then((res) => res.data)
      .then((data) => {
        console.log(data);
        this.setState({
          selectedProduct: "",
          isElementClicked: false,
          titleField: "",
          descriptionField: "",
          priceField: "",
          imageField: "",
          categoryField: "",
          updateFetchError: false,
        });
      })
      .catch((err) => {
        this.setState({
          updateFetchError: true,
          updateFetcherrMessage: "Error occured. Please enter a valid URL",
        });
      });
  };

  getEnteredProductFields = () => {
    this.setState({
      isFetchDataFetched: true,
    });
    const createProductObj = {
      title: this.state.titleTobeAdded,
      price: this.state.priceToBeAdded,
      description: this.state.descriptionTobeAdded,
      image: this.state.ImageTobeAdded,
      category: this.state.categoryTobeAdded,
    };

    axios
      .post("https://fakestoreapi.com/products", createProductObj)
      .then((res) => res.data)
      .then((data) => {
        console.log(data);
        this.setState({
          isFetchDataFetched: false,
          FetchedData: [...this.state.FetchedData, data],
          isCreateFetchFailed: false,
          createFetchSuccess: "Product Created Sucessfully",
        });
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          isFetchDataFetched: false,
          isCreateFetchFailed: true,
          createFetchError: "Error occured. Please provide correct URL",
          createFetchSuccess: "",
        });
      });
  };

  deleteFn = () => {
    console.log("delete triggered");
    this.setState({
      isFetchDataFetched: true,
    });

    let rawData = this.state.FetchedData;
    console.log(rawData);
    axios
      .delete(
        `https://fakestoreapi.com/products/${this.state.prodTobeDeleted}`,
        {}
      )
      .then((res) => res.data.id)
      .then((data) => {
        console.log(data);
        const filteredData = rawData.filter((eachProd) => eachProd.id !== data);
        this.setState(
          {
            isFetchDataFetched: false,
            FetchedData: filteredData,
            deleteFetchError: false,
            DeleteSuccessMessage: "Delete Product Successful",
            deleteFailMessage: "",
          },
          () => console.log(this.state.FetchedData)
        );
      })
      .catch((err) => {
        this.setState({
          isFetchDataFetched: false,
          deleteFetchError: true,
          DeleteSuccessMessage: "",
          deleteFailMessage:
            "Error Occured. Please enter a valid ID.",
        });
      });
  };

  deleteId = (val) => {
    console.log(val);
    console.log("deleteId triggered");
    this.setState(
      {
        prodTobeDeleted: val,
      },
      () => console.log(this.state.prodTobeDeleted)
    );
  };

  render() {
    return (
      <BrowserRouter>
        <HeaderContent />
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <AllProducts
                updateTitle={this.updateTitle}
                updateDescription={this.updateDescription}
                updatePrice={this.updatePrice}
                updateImage={this.updateImage}
                updateCategory={this.updateCategory}
                onSaveFn={this.onSaveFn}
                isFetchingFailed={this.state.isFetchingFailed}
                isDataFetched={this.state.isDataFetched}
                FetchedData={this.state.FetchedData}
                selectedProduct={this.state.selectedProduct}
                titleField={this.state.titleField}
                descriptionField={this.state.descriptionField}
                priceField={this.state.priceField}
                categoryField={this.state.categoryField}
                imageField={this.state.imageField}
                isElementClicked={this.state.isElementClicked}
                getClickedProductDetails={this.getClickedProductDetails}
                updateFetchError={this.state.updateFetchError}
                updateFetcherrMessage={this.state.updateFetcherrMessage}
              />
            )}
          />
          <Route
            exact
            path="/createproduct"
            render={() => (
              <CreateProduct
                isCreateFetchFailed={this.state.isCreateFetchFailed}
                createFetchError={this.state.createFetchError}
                createFetchSuccess={this.state.createFetchSuccess}
                titleTobeAdded={this.state.titleTobeAdded}
                priceToBeAdded={this.state.priceToBeAdded}
                descriptionTobeAdded={this.state.descriptionTobeAdded}
                ImageTobeAdded={this.state.ImageTobeAdded}
                categoryTobeAdded={this.state.categoryTobeAdded}
                newTitle={this.newTitle}
                newDescription={this.newDescription}
                newPrice={this.newPrice}
                newImage={this.newImage}
                newCategory={this.newCategory}
                getEnteredProductFields={this.getEnteredProductFields}
                isFetchDataFetched={this.state.isFetchDataFetched}
              />
            )}
          />
          <Route
            path="/deleteproduct"
            render={() => (
              <DeleteProduct
                prodTobeDeleted={this.state.prodTobeDeleted}
                deleteFn={this.deleteFn}
                deleteId={this.deleteId}
                deleteFetchError={this.state.deleteFetchError}
                DeleteSuccessMessage={this.state.DeleteSuccessMessage}
                deleteFailMessage={this.state.deleteFailMessage}
                FetchedData={this.state.FetchedData}
                isFetchDataFetched={this.state.isFetchDataFetched}
              />
            )}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
