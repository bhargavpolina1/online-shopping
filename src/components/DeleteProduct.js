import React, { Component } from "react";

class DeleteProduct extends Component {
  deleteIdHere = (event) => {
    this.props.deleteId(event.target.value);
  };
  render() {
    return (
      <div className="container">
        <div className="prodTobeDeletedrow">
          <div className="col-12 col-md-6">
            <label htmlFor="exampleInputEmail1">
              Enter the ID of the product to be deleted
            </label>
            <input
              type="number"
              onChange={this.deleteIdHere}
              value={this.props.prodTobeDeleted}
              className="form-control"
            />
            <button
              className="btn btn-primary m-1"
              onClick={this.props.deleteFn}
            >
              Delete Product
            </button>
            {this.props.isFetchDataFetched ? <div className="spinner-border text-primary" role="status"></div>
            : this.props.deleteFetchError ? (
              <p>{this.props.deleteFailMessage}</p>
            ) : (
              <p>{this.props.DeleteSuccessMessage}</p>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteProduct;
