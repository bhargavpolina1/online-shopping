import React, { Component } from "react";
import ProductCard from "./productCard";
import Eachproduct from "./Eachproduct";
import "./Products.css";

class AllProducts extends Component {
  render() {
    if (this.props.isDataFetched) {
      let fetchedLength = this.props.FetchedData.length;
      if (!fetchedLength) {
        return <div>No Products</div>;
      } else {
        return (
          <div className="container-fluid productsMainContainer">
            <div className="row">
              <div className="col-6 d-flex flex-wrap productCardContainer">
                {this.props.FetchedData.map((eachProduct) => {
                  return (
                    <div
                      className="col-6 col-md-4 col-lg-3 mt-5 text-white"
                      key={eachProduct.id}
                    >
                      <ProductCard
                        eachProduct={eachProduct}
                        getClickedProductDetails={
                          this.props.getClickedProductDetails
                        }
                      />
                    </div>
                  );
                })}
              </div>
              <div className="col-6 d-flex flex-column justify-content-center">
                {this.props.isElementClicked ? (
                  <Eachproduct
                    selectedProduct={this.props.selectedProduct}
                    updateTitle={this.props.updateTitle}
                    updateDescription={this.props.updateDescription}
                    updatePrice={this.props.updatePrice}
                    updateImage={this.props.updateImage}
                    updateCategory={this.props.updateCategory}
                    onSaveFn={this.props.onSaveFn}
                    descriptionField={this.props.descriptionField}
                    titleField={this.props.titleField}
                    priceField={this.props.priceField}
                    categoryField={this.props.categoryField}
                    imageField={this.props.imageField}
                    updateFetchError={this.props.updateFetchError}
                    updateFetchErrMessage={this.props.updateFetchErrMessage}
                  />
                ) : (
                  <h1>Please select a product to edit</h1>
                )}
              </div>
            </div>
          </div>
        );
      }
    } else if (this.props.isFetchingFailed) {
      return (
        <div className="errorContainer">
          <div className="errorImageContainer mt-5">
            <img
              className="errorImage"
              src="https://sitechecker.pro/wp-content/uploads/2017/12/404.png"
              alt="error"
            />
          </div>
          <p>There is an error while fetching data</p>
        </div>
      );
    } else {
      return (
        <div className="spinnerContainer">
          <div className="spinner-border text-primary" role="status"></div>
        </div>
      );
    }
  }
}

export default AllProducts;
