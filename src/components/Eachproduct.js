import React, { Component } from "react";

class Eachproduct extends Component {

  callSave = (givenId) => {
    this.props.onSaveFn(givenId)
  }
  render() {
    const {id,image, title, category, description, price, rating } =
      this.props.selectedProduct;
    return (
      <div className="d-flex flex-column align-items-center eachProductMainContainer">
        <div className="d-flex flex-column align-items-center m-1 productIndContainer  bg-white">
          <div className="eachImageContainer">
            <img className="eachProductImage" src={image} alt="img"></img>
          </div>
          <div className="d-flex flex-column p-1 pt-2 justify-content-center align-items-center">
            <h1 className="eachProductName">{title}</h1>
            <span className="eachProductCategory">{category}</span>
            <br />
            <span className="eachProductDescription">{description}</span>
            <br />
            <span className="eachProductPrice">${price}</span>
            <div className="d-flex">
              <span className="eachProductRate">Rating: {rating.rate}</span>
              <span className="eachRateCount">Rated By: {rating.count}</span>
            </div>
          </div>
        </div>
        <div className="col-6">
          <h3>Edit the details below</h3>
                <label htmlFor="exampleInputEmail1">Title</label>
                <input
                  type="text"
                  onChange={this.props.updateTitle}
                  value={this.props.titleField}
                  className="form-control"
                />
                <label htmlFor="exampleInputPassword1">Description</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.props.descriptionField}
                  onChange={this.props.updateDescription}
                />
                <label htmlFor="exampleInputPassword1">Price</label>
                <input
                  type="number"
                  className="form-control"
                  value={this.props.priceField}
                  onChange={this.props.updatePrice}
                />
                <label htmlFor="exampleInputPassword1">Image URL</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.props.imageField}
                  onChange={this.props.updateImage}
                />
                <label htmlFor="exampleInputPassword1">Category</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.props.categoryField}
                  onChange={this.props.updateCategory}
                />
                <button
                  className="btn btn-primary mt-2"
                  onClick={() => this.callSave(id)}
                >
                  Save
                </button>
                {this.props.updateFetchError ?<p>{this.props.updateFetcherrMessage}</p> : null}
              </div>
      </div>
    );
  }
}

export default Eachproduct;
