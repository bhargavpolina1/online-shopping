import React, { Component } from "react";

class CreateProduct extends Component {
constructor(props) {
  super(props)
  this.onCreateProduct = this.onCreateProduct.bind(this);
}


onCreateProduct(){
  this.props.getEnteredProductFields()  
}

titleChange=(event)=>{
  this.props.newTitle(event.target.value)
}

descriptionChange=(event)=>{
  this.props.newDescription(event.target.value)
}

priceChange=(event)=>{
  this.props.newPrice(event.target.value)
}

imageChange=(event)=>{
  this.props.newImage(event.target.value)
}

categoryChange=(event)=>{
  this.props.newCategory(event.target.value)
}

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-6">
            <h3>Enter the details of the product below</h3>
            <label htmlFor="exampleInputEmail1">Title</label>
            <input type="text" onChange={this.titleChange} value = {this.props.titleTobeAdded} className="form-control" />
            <label htmlFor="exampleInputPassword1">Price</label>
            <input type="number" onChange={this.priceChange} value = {this.props.priceTobeAdded} className="form-control" />
            <label htmlFor="exampleInputPassword1">Description</label>
            <input type="text" onChange={this.descriptionChange} value = {this.props.descriptionTobeAdded} className="form-control" />
            <label htmlFor="exampleInputPassword1">Image URL</label>
            <input type="text" onChange={this.imageChange} value = {this.props.imageTobeAdded} className="form-control" />
            <label htmlFor="exampleInputPassword1">Category</label>
            <input type="text" onChange={this.categoryChange} value = {this.props.categoryTobeAdded} className="form-control" />
            <button className="btn btn-primary mt-2" onClick={this.onCreateProduct}>
              Create Product
            </button>
            {this.props.isFetchDataFetched ? <div className="spinner-border text-primary" role="status"></div>:
            this.props.isCreateFetchFailed ?<p>{this.props.createFetchError}</p>:<p>{this.props.createFetchSuccess}</p>}
            
          </div>
        </div>
      </div>
    );
  }
}

export default CreateProduct;
