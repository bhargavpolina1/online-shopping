import React, { Component } from "react";
import "./Products.css";

class ProductCard extends Component {
    constructor(props) {
      super(props)

      this.changeDetails = this.changeDetails.bind(this)
    } 

    changeDetails=(event)=>{
        
        this.props.getClickedProductDetails(event.target.id)
      
    
    }
  render() {
    const {id,title, image, category, price } = this.props.eachProduct;
    return (
      <div className="d-flex flex-column align-items-center m-1 bg-primary productCardContainer">
        <div className="imageContainer">
          <img className="productImage" src={image} alt="img"></img>
        </div>
        <div className="d-flex flex-column p-1 pt-2 justify-content-center align-items-center">
          <h6 className="productName">{title}</h6>
          <span className="productCategory">{category}</span>
          <br />
          <span className="productPrice">${price}</span>
        </div>
        <button className="btn bg-white w-75 editButton" id = {id} onClick={this.changeDetails}>Click to Edit</button>
      </div>
    );
  }
}

export default ProductCard;
