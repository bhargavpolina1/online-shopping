import React, { Component } from "react";
import { Link } from "react-router-dom";
/* import "./Header.css"; */
class HeaderContent extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-md navbar-dark bg-primary">
        <div className="container">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link to="/" className="nav-link">
                  Home
                </Link>
              </li>
                <li className="nav-item">
                <Link to="/createproduct" className="nav-link">
                  Create Product
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/deleteproduct" className="nav-link">
                  Delete Product
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default HeaderContent;